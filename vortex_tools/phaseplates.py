import numpy as np
from vortex_tools.utils import normalise, testY
from vortex_tools import electron

def biprism(dphase: float, X: np.ndarray, Y=None, rotation=0)-> np.ndarray:
    """
    Creates a phase plate with a triangular profile (like that of a triangular biprism).
    The phase plate is zero in the middle of the image and increases (decreases) towards the edges of the image

    Parameters
    ----------
    dphase : float
        slope of the phase ramp, in rad/unit of X and Y

    X : np.ndarray
        meshgrid with the values of the x coordinate
    Y : np.ndarray 
        meshgrid with the values of the y coordinate. If missing, transposed X is used
    rotation : float
        azimuthal angle indicating the orientation of the biprism's symmetry axis, expressed in radians.

    Returns
    np.ndarray
        complex transmission function of the phase plate
    """
    
    Y = testY(X,Y)
    R = np.sqrt(X**2 + Y**2)
    angle = np.arctan2(Y,X)
    plate = np.exp(1j*dphase *R*np.abs(np.cos(angle+rotation)))
    
    return plate

def oam_plate(ell: float, X: np.ndarray, Y=None, rotation: float = 0)-> np.ndarray:
    """
    Creates a phase plate with a triangular profile (like that of a triangular biprism).
    The phase plate is zero in the middle of the image and increases (decreases) towards the edges of the image

    Parameters
    ----------
    ell : float
        value of OAM. In all normal cases an integer is recommended, but a float will also be used though it will cause a discontinuity.
    X : np.ndarray
        meshgrid with the values of the x coordinate
    Y : np.ndarray 
        meshgrid with the values of the y coordinate. If missing, transposed X is used
    rotation : float
        azimuthal angle indicating the orientation of the biprism's symmetry axis, expressed in radians. Only has a real impact for non-integer ell

    Returns
    -------
    np.ndarray
        complex transmission function of the phase plate
    """

    Y = testY(X,Y)
    angle = np.arctan2(Y,X)
    plate = np.exp(1j*(angle+rotation)*ell)

    return plate


def tilt_plate(theta: float, X: np.ndarray, Y=None, wavelength=None, E0: float =3e5)-> np.ndarray:

    Y = testY(X,Y)
    theta = np.array(theta)
    k_par = electron.k0(wavelength = wavelength, E0 = E0)
    dk = theta*k_par
    spacing = 2*np.pi/dk
    plate = 2*np.pi *(X/spacing[0] + Y/spacing[1])
    return plate


def aberrations_seidel(coefficients: dict, KX: np.ndarray, KY: np.ndarray = None, wavelength: float = None, E0: float = 3e5)-> np.ndarray :
    """Computes the aberration phae plate for an arbitrary set of aberration coefficients.

    Parameters
    ----------
    coefficients : dict
        Dictionary containing the coefficients for the Seidel aberrations in the CEOS notation
    KX : np.ndarray
        [description]
    KY : np.ndarray, optional
        [description], by default None
    wavelength : float, optional
        [description], by default None
    E0 : float, optional
        [description], by default 3e5

    Returns
    -------
    np.ndarray
        [description]

    Examples
    --------
    >>> ab_dict = {"A1":15e-9, "Phi22": np.pi/4, "C1": 24e-9, "C3": 1.2e-3}
    >>> aberration_seidel(coefficients = ab_dict, KX, KY, E0=120e3)
    """
    KY = testY(KX,KY)
    k_par = electron.k0(wavelength=wavelength, E0=E0)
    Theta = np.sqrt(KX**2+KY**2)/k_par
    Phi = np.arctan2(KY,KX)
    chi = np.zeros(Theta.shape)
    for aberration in aberration_list :
        radial = aberration["radial"]
        azimuthal = aberration["azimuthal"]
        try : # in a try, so if one or both coefficients are missing we don't create the plate
            strength = coefficients[aberration["name"]]
            if aberration["namerotation"] != None :
                rotation = coefficients[aberration["namerotation"]]
            else:
                rotation = 0
            partialplate = k_par/radial* Theta**radial* strength*np.cos( azimuthal*(Phi-rotation) )
            chi += partialplate
        except KeyError:
            pass
    return np.exp(1j*chi)



    



aberration_list = [{ "name":"A1", "namerotation":"Phi22", "radial":2, "azimuthal":2 },
                   { "name":"C1", "namerotation": None  , "radial":2, "azimuthal":0 },
                   { "name":"A2", "namerotation":"Phi33", "radial":3, "azimuthal":3 },
                   { "name":"B2", "namerotation":"Phi31", "radial":3, "azimuthal":1 },
                   { "name":"A3", "namerotation":"Phi44", "radial":4, "azimuthal":4 },
                   { "name":"S3", "namerotation":"Phi42", "radial":4, "azimuthal":2 },
                   { "name":"C3", "namerotation": None  , "radial":4, "azimuthal":0 },
                   { "name":"A4", "namerotation":"Phi55", "radial":5, "azimuthal":5 },
                   { "name":"D4", "namerotation":"Phi53", "radial":5, "azimuthal":3 },
                   { "name":"B4", "namerotation":"Phi51", "radial":5, "azimuthal":1 }]