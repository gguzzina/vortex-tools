import numpy as np
from vortex_tools.utils import normalise, testY
import vortex_tools.phaseplates as plates
 
def pinhole(radius, X, Y=None, center=(0,0)):
    """Creates a pinhole centered in center, with given radius, and using coordinates X and Y

    Parameters
    ----------
    radius : float
        radius of the pinhole
    X : array
        meshgrid with the values of the x coordinate
    Y : array, optional
        meshgrid with the values of the y coordinate. If missing, transposed X is used
    center : tuple of floats
        coordinates of the center of the pinhole, useful to make off-center pinholes


    Returns
    -------
    array
        array with the transmission function of the desired pinhole.
    """
    Y = testY(X,Y)

    R = np.sqrt( (X-center[0])**2+(Y-center[1])**2)
    pinh = np.zeros(R.shape)+1e-5
    pinh[R < radius] = 1
    pinh = normalise(pinh)
    return pinh

def oam_aperture(radius, ell, X, Y=None, rotation = 0):
    """Creates a pinhole centered in center, with given radius, and using coordinates X and Y

    Parameters
    ----------
    radius : float
        radius of the aperture
    ell : float
        value of the topological charge. An integer is recommended, but a float will also be used though it will cause a discontinuity.
    X : array
        meshgrid with the values of the x coordinate
    Y : array, optional
        meshgrid with the values of the y coordinate. If missing, transposed X is used
    rotation : float
        azimuthal angle indicating the orientation of the biprim's symmetry axis, expressed in radians. Only has a real impact for non-integer ell


    Returns
    -------
    array
        array with the complex transmission function of the desired aperture.
    """
    Y = testY(X,Y)

    pinh = pinhole(radius, X, Y)
    plate = plates.oam_plate(ell, X, Y, rotation=rotation)
    aperture = pinh*plate
    aperture = normalise(aperture)
    return aperture
