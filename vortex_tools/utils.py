import numpy as np
from numpy import pi
from matplotlib import pyplot as plt
from colorsys import hls_to_rgb
import matplotlib

def colorize(z):
    ### Very handy function thanks to used nadapez on Stack OVerflow, source: https://stackoverflow.com/a/20958684
    z = z/(np.abs(z).max())
    r = np.abs(z)**2
    arg = np.angle(z) 

    h = (arg + np.pi)  / (2 * pi) + 0.5
    l = 1.0 - 1.0/(1.0 + r**0.6)
    s = 0.8

    c = np.vectorize(hls_to_rgb) (h,l,s) # --> tuple
    c = np.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
    c = c.swapaxes(0,2) 
    return c

def compleximage(cimg, X=None, Y=None, show=True) -> matplotlib.image.AxesImage:
    """ Plot a complex array using the amplitude for the colour intensity, and the phase for the hue
    
    Parameters
    ----------
    cimg : array
        Complex array to be plotted
    X : array, optional
        meshgrid for X coordinate, default None
    Y : array, optional
        meshgrid for X coordinate, default None
    show : bool, optional
        Should the plot be displayed? Default True
    """
    extent = [0,cimg.shape[0],0,cimg.shape[1]]
    img = colorize(cimg)
    plot = plt.show()
    if isinstance(X, np.ndarray):
        extent[0] = X[0,0]
        extent[1] = X[-1,-1]
    if isinstance(Y, np.ndarray):
        extent[2] = Y[0,0]
        extent[3] = Y[-1,-1]
        
    if show == True:
        plot = plt.imshow(img, extent=extent)
    
    return plot

def normalise(array: np.ndarray) -> np.ndarray:
    """ Normalises the given array so that the integral of the squared modulus is equal to 1

    Parameters
    ----------
    array : np.ndarray
        numpy ndarray to be normalised

    Returns
    -------
    np.ndarray
        normalised version of the input array

    Examples
    --------
    >>> normarray = normalise(array)
    >>> numpy.abs(normarray)**2).sum() == 1
    True
    """
    return array/np.sqrt((np.abs(array)**2).sum())


def testY(X,Y):
    try :
        if Y == None : 
            Y = X.T
    except ValueError as err:
        if isinstance(Y, np.ndarray):
            if Y.shape == X.shape:
                pass
            else:
                raise ValueError('Size Mismatch')
        else:
            raise err
    return Y