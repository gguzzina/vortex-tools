import numpy as np
from vortex_tools.electron import *

def init_rspace(pixel, npointsx = 2048, npointsy = None):
    """ Creates a meshgrid for real space X and Y, with the given pixel size and the sizes npointsx and npointsy.

    Parameters
    ----------
    pixel : float
        pixel size. Please use meters
    npointsx : float
        number of pixels along X. Default is 2048
    npointsy : float
        number of pixels along Y. Default is npointsx
    
    Returns
    -------
    X : np.ndarray
        meshgrid with the values of the x coordinate
    Y : np.ndarray
        meshgrid with the values of the y coordinate
    """
    if npointsy == None :
        npointsy = npointsx
    xmax = pixel*npointsx/2
    ymax = pixel*npointsy/2
    x = np.linspace(-xmax,xmax, npointsx, endpoint=False)
    y = np.linspace(-ymax,ymax, npointsy, endpoint=False)
    X, Y = np.meshgrid(x,y)
    return X,Y

def init_kspace(pixel: float, npointsx: int = 2048, npointsy: int = None , wavelength: float = None, E0: float = 3e5) -> tuple:
    """ Creates a meshgrid for reciprocal space KX and KY, with the given pixel size and the sizes npointsx and npointsy.
    The pixel size is given as angle, and either wavelength or E0 is required, or values are given for 300kV.

    Parameters
    ----------
    pixel : float
        pixel size, radians
    npointsx : float, optional
        number of pixels along KX. Default is 2048
    npointsy : float, optional
        number of pixels along Y, if different from npointsx. Default is None, and npointsx is used instead
    wavelength : float, optional
        de Broglie wavelength, meters. Default is None and the wavelength is computed from kinetic energy.
    E0 : float, optional
        kinetic energy of the electrons. Used to compute wavelength. It's ignored if wavelength is specified. Default is 300kV
    
        
    Returns
    -------
    KX : np.ndarray
        meshgrid containing the X momentum coordinates
    KY : np.ndarray
        meshgrid containing the Y momentum coordinates
    """
    if wavelength != None:
        wvlgt = wavelength
    else:
        wvlgt = wavelength_relativistic(E0)
    k_par = k0(wavelength = wvlgt)

    if npointsy == None :
        npointsy = npointsx
        
    kxmax = k_par*pixel*npointsx/2
    kymax = k_par*pixel*npointsy/2
    kx = np.linspace(-kxmax,kxmax, npointsx, endpoint=False)
    ky = np.linspace(-kymax,kymax, npointsy, endpoint=False)
    KX, KY = np.meshgrid(kx,ky)
    return KX, KY

def physroll(shift: tuple, a: np.ndarray, X: np.ndarray, Y: np.ndarray = None)-> np.ndarray:
    """ A wrapper for np.roll using calibrated units. Shifts an array by units calibrated using the supplied meshgrids.
    The shift will be converted to calibrated pixel, and rounded to the nearest pixel.

    Parameters
    ----------
    shift : tuple of floats
        (x,y) components of the shift vector to apply to the array. The shi
    a : np.ndarray
        array to be shifted
    X : np.ndarray
        meshgrid with the values of the x coordinate
    Y : np.ndarray 
        meshgrid with the values of the y coordinate. If missing, transposed X is used

    Returns
    -------
    np.ndarray
        shifted array
    """

    Y = testY(X,Y)

    px = X[1,1]-X[0,0]
    py = Y[1,1]-Y[0,0]
    shiftx = round(shift[0]/px)
    shifty = round(shift[1]/py)
    a = np.shift(a, (shifty,shiftx), (0,1))
    return a