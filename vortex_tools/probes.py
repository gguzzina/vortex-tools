import numpy as np
from vortex_tools.utils import normalise
import vortex_tools.phaseplates as plates
import vortex_tools.apertures as apertures
import vortex_tools.electron as electron
import vortex_tools.waveoptics as waves
from vortex_tools.utils import testY

def airy_disc(alpha, X, Y=None, wavelength=None, E0=3e5):
    """Generate an airy disk with the specified convergence angle

    Parameters
    ----------
    alpha : float
        semi-convergence angle
    X : array
        meshgrid with the values of the x coordinate
    Y : array, optional
        meshgrid with the values of the y coordinate. If missing, transposed X is used
    wavelength : float, optional
        de Broglie wavelength, meters. Default is None and the wavelength is computed from kinetic energy.
    E0 : float, optional
        kinetic energy of the electrons. Used to compute wavelength. It's ignored if wavelength is specified. Default is 300kV

    Returns
    -------
    [type]
        [description]
    """

    Y = testY(X,Y)
    _, KX,KY = waves.physfft(X,X,Y)
    k0 = electron.k0(wavelength = wavelength, E0 = E0)
    pinh = apertures.pinhole(alpha*k0, KX, KY)
    disc = waves.physifft(pinh, KX, KY)
    return disc

def oam_probe(alpha, ell, X, Y=None, rotation = 0, wavelength=None, E0=3e5):
    """Generate an airy disk with the specified convergence angle

    Parameters
    ----------
    alpha : float
        semi-convergence angle
    ell : float
        value of OAM. In all normal cases an integer is recommended, but a float will also be used though it will cause a discontinuity.
    X : array
        meshgrid with the values of the x coordinate
    Y : array, optional
        meshgrid with the values of the y coordinate. If missing, transposed X is used
    rotation : float
        allows to azimuthally rotate the phase plate. Only really has an effect with non-integer ell
    wavelength : float, optional
        de Broglie wavelength, meters. Default is None and the wavelength is computed from kinetic energy.
    E0 : float, optional
        kinetic energy of the electrons. Used to compute wavelength. It's ignored if wavelength is specified. Default is 300kV

    Returns
    -------
    np.array
        complex wavefunction of the beam
    """

    Y = testY(X,Y)
    _, KX,KY = waves.physfft(X,X,Y)
    k0 = electron.k0(wavelength = wavelength, E0 = E0)

    aperture = apertures.oam_aperture(alpha*k0, ell, KX, KY, rotation=rotation)
    vortex, _, _, = waves.physifft(aperture, KX, KY, coordinates = False)
    return vortex

    
def gaussian(alpha, X, Y=None, wavelength=None, E0=3e5):
    """Generate a gaussian probe with the given convergence angle

    Parameters
    ----------
    alpha : float
        semi-convergence angle
    X : np.array
        meshgrid with the values of the x coordinate
    Y : np.array, optional
        meshgrid with the values of the x coordinate. If missing, the transpose of X is used.
    wavelength : float, optional
        de Broglie wavelength, meters. Default is None and the wavelength is computed from kinetic energy.
    E0 : float, optional
        kinetic energy of the electrons. Used to compute wavelength. It's ignored if wavelength is specified. Default is 300kV

    Returns
    -------
    np.array
        complex wavefunction of the beam
     """

    Y = testY(X,Y)
    if wavelength == None:
        wavelength = electron.wavelength_relativistic(E0)
    w0 = wavelength/(alpha*np.pi)
    R2 = X**2 + Y**2
    probe = np.exp(-R2/w0**2)
    return probe