import numpy as np
from matplotlib import pyplot as plt
from vortex_tools import electron as electron
from vortex_tools.utils import normalise, testY

def physfft(wave: np.ndarray, X: np.ndarray, Y: np.ndarray = None, coordinates: bool = True) -> tuple:
    """ Does a 2d fourier transform, from real to reciprocal taking care of appropriate calibration.

    Parameters
    ----------
    wave: np.ndarray
        complex 2D wave to transform
    X : np.ndarray
        meshgrid containing the X coordinates
    Y : np.ndarray
        meshgrid containing the Y coordinates
    coordinates : bool
        should this script also produce momentum coordinates?

    Returns
    -------
    farfield : np.ndarray
        complex diffraction pattern of the input wave
    KX : np.ndarray
        meshgrid containing the X momentum coordinates
    KY : np.ndarray
        meshgrid containing the Y momentum coordinates

    """
    Y = testY(X,Y)

    if wave.shape != X.shape or wave.shape != Y.shape :
        raise Exception("The arrays are not the same size")
        
    shp  = wave.shape
    ptsx = shp[0]
    ptsy = shp[1]
    x    = X[0,:].T
    y    = Y[:,0].T
    dx   = x[1]-x[0]
    dy   = y[1]-y[0]
    
    if coordinates == True:
        KmaxX= np.pi/dx
        KmaxY= np.pi/dy
        kx   = np.linspace(-KmaxX,KmaxX,ptsx, endpoint=False)
        ky   = np.linspace(-KmaxY,KmaxY,ptsy, endpoint=False)
        KX,KY = np.meshgrid(kx,ky)
    else :
        KX, KY = None, None
    farfield = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(wave)))
    #farfield = normalise(farfield)
    return farfield, KX, KY


def physifft(wave: np.ndarray, KX: np.ndarray, KY: np.ndarray, coordinates: bool = True) -> tuple:
    """ Does a 2d fourier transform from reciprocal to real space, taking care of appropriate calibration.

    Parameters
    ----------
    wave: np.ndarray
        complex 2D wave to transform
    KX : np.ndarray
        meshgrid containing the X momentum coordinates
    KY : np.ndarray
        meshgrid containing the Y momentum coordinates
    
    Returns
    -------
    farfield : np.ndarray
        complex diffraction pattern of the input wave
    KX : np.ndarray
        meshgrid containing the X momentum coordinates
    KY : np.ndarray
        meshgrid containing the Y momentum coordinates    
    """
    KY = testY(KX,KY)

    if wave.shape != KX.shape or wave.shape != KY.shape :
        raise Exception("The arrays are not the same size")
    
    shp  = wave.shape
    ptsx  = shp[0]
    ptsy  = shp[1]
    kx    = KX[0,:].T
    ky    = KY[:,0].T
    dkx   = kx[1]-kx[0]
    dky   = ky[1]-ky[0]
    
    if coordinates == True:
        Xmax= np.pi/dkx
        Ymax= np.pi/dky
        x   = np.linspace(-Xmax,Xmax,ptsx, endpoint=False)
        y   = np.linspace(-Ymax,Ymax,ptsy, endpoint=False)
        X,Y = np.meshgrid(x,y)
    else:
        X, Y = None, None
    infocus = np.fft.ifftshift(np.fft.ifft2(np.fft.fftshift(wave)))
    #infocus = normalise(infocus)
    return infocus, X,Y


def propagator(df, KX, KY=None, E0=3e5, wavelength=None):
    
    if wavelength != None:
        wvlgt = wavelength
    else:
        wvlgt = electron.wavelength_relativistic(E0)
    k_par = electron.k0(wavelength=wvlgt)

    KY = testY(KX,KY)
    K2 = KX**2+KY**2
    Fz = np.exp(1j*K2*df*0.5/k_par) #This is the actual Frenel propagator in fourier space
    return Fz



def propagate(wave: np.ndarray, df: float, X: np.ndarray, Y: np.ndarray = None, E0: float = 3e5, wavelength: float = None):
    """Applies the fresnel propagator to propagate the given wave to the distance df 

    Parameters
    ----------
    wave : np.ndarray
        the wavefront to be propagated
    df : float
        the propagation distance
    X : np.ndarray
        [description]
    Y : np.ndarray, optional
        [description], by default None
    E0 : float, optional
        [description], by default 3e5
    wavelength : float, optional
        [description], by default None

    Returns
    -------
    [type]
        [description]
    """

    
    Y = testY(X,Y)
    Fourierwave, KX, KY = physfft(wave, X, Y)
    Fz = propagator(df, KX, KY, E0 = E0, wavelength = wavelength)
    propagated_wave = physifft(Fourierwave*Fz,KX,KY)[0]
    return propagated_wave

