import numpy as np
from scipy import constants as cst

#e_charge = 1.60217662e-19 #coulombs
#hbar = 1.0545718e-34 #m2 kg / s
#hplanck = 2*hbar*np.pi
#c = 299792458 # m/s
#m0 = 9.10938356e-31 #kg
E_rest = cst.physical_constants['electron mass energy equivalent in MeV'][0]*1e6
 
def k0(E0 = 3e5, wavelength = None):
    """ Returns the forward momentum of an electron.
    Just use either optional argument. If neither is given, return k0 for 300kV

    Parameters
    ----------
    E0 : float, optional
        Kinetic energy in eV, defaulyt 300kV
    wavelength : float, optional
        Electron wavelength in meters
    """
    if wavelength != None:
        wvlgt = wavelength
    else:
        wvlgt = wavelength_relativistic(E0)
    k_par = 2*np.pi/wvlgt
    return k_par


def wavelength_relativistic(E0 = 3e5):
    """Returns the relativiscally corrected wavelength for a given kinetic energy.
    Parameters
    ----------
    E0 : float, optional
        Kinetic energy in eV, defaulyt 300kV
    """
    E_tot = E_rest+E0
    wavelength = cst.h*cst.c/cst.e/np.sqrt(E_tot**2-E_rest**2)
        
    return wavelength

def beta_lorentz(E0 = 3e5):
    """Returns the relativisc speed beta for a given kinetic energy.
    
    Parameters
    ----------
    E0 : float, optional
        Kinetic energy in eV, defaulyt 300kV
    """
    beta = np.sqrt(1-(1/gamma_lorentz(E0)**2))
        
    return beta

def gamma_lorentz(E0 = 3e5):
    """Returns the relativiscally factor gamma for a given kinetic energy.
    
    Parameters
    ----------
    E0 : float, optional
        Kinetic energy in eV, defaulyt 300kV
    """
    gamma = (E_rest+E0)/E_rest
    
    return gamma

def inelastic_angle_nonrelativistic(dE, E0 = 3e5):
    """Returns the characteristic inelastic scattering angle, without relativistic corrections.

    Parameters
    ----------
    dE : float
        energy loss in eV
    E0 : float, optional
        Kinetic energy in eV, defaulyt 300kV
    """
    theta = dE/(2*E0)
    return theta



def inelastic_delocalisation(dE, E0 = 3e5):
    """Returns the length of the inelastic delocalisation. for the energy loss dE and primary energy E0.

    Parameters
    ----------
    dE : float
        energy loss in eV
    E0 : float, optional
        Kinetic energy in eV, defaulyt 300kV
    """
    L = 0.5*wavelength_relativistic(E0)/(inelastic_angle_nonrelativistic(dE,E0)**0.75)
    return L


