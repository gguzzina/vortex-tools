from setuptools import setup, find_packages

setup(
        name = 'vortex_tools',
        packages = [
            'vortex_tools',
            ],
        version = '0.2.1',
        description = 'Numerical scripts for (scalar) paraxial optics. Written with the use for electrons in mind.',
        author = 'Giulio Guzzinati',
        author_email = 'giulio.guzzinati@uantwerpen.be',
        license = 'GPL v3',
        keywords = [
            'optics',
            'electron optics',
            'microscopy',
            'electron vortex beams'
            ],
        install_requires = [
            'numpy>=1.13',
            'matplotlib>=3.1.0',
            'scipy',
            'tqdm',
            ],
        classifiers = [
            'Development Status :: 3 - Alpha',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Intended Audience :: Science/Research',
            'Programming Language :: Python :: 3',
            ],
        package_data = {
            }
)
