# Vortex Tools: a collection of wave optics and electron optics helper scripts

This package contains a variety of scripts and functions to perform common tasks in building simple paraxial optics simulations such as:
- a 2D fft wrapper with correct fftshift use and coordinate calibration
- plotting of complex images with phase as hue and amplitude as saturation
- generation of aperture shapes
- generation of an aberration phase plate for selected coefficients

Also, a number of helper functions to help with back of the envelope calculations are included, such as:
- Lorentz beta and gamma
- Calculation of relativistic wavelength or momentum from energy
- Inelastic delocalisation estimation


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for usage and development.

### Prerequisites

This software has common dependencies:
- python > 3.6
- numpy > 1.13
- matplotlib > 3.1.0
- scipy


### Installing

The easiest thing is to install a recent version of anaconda.
Then you can install the sources by using
```bash
pip install .
```
Then you can just import the library with:
```python
from vortex_tools import api as vrt
```

## Contributing

Just get in touch however you please. There's plenty of room for improvement across the board.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/gguzzina/vortex-tools/tags).

## Author

* [**Giulio Guzzinati**](@gguzzina)

See also the list of [contributors](https://gitlab.com/gguzzina/vortex-toolscontributors) who participated in this project.

## License

This project is licensed under the GPLv3 license - see the [LICENSE.txt](LICENSE.txt) file for details
